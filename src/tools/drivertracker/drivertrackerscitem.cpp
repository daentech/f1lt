/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "drivertrackerscitem.h"

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include "../../core/eventdata.h"

namespace DriverTrackerTool
{

DriverTrackerSCItem::DriverTrackerSCItem(DriverTrackerTimer *timer, QGraphicsItem *parent) :
    QGraphicsObject(parent), dtTimer(timer), leaderDriver(NULL), viewScale(1.0), viewRotation(0)
{
    scHelmet = QPixmap(":/ui_icons/sc_helmet.png").scaledToHeight(40, Qt::SmoothTransformation);
    setZValue(1.5);
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}

QRectF DriverTrackerSCItem::boundingRect() const
{

    int size = 40;

    if (viewScale >= 2.0)
    {
        size = 80;
    }
    else if (viewScale > 1.0)
    {
        size = 40 * viewScale;
    }

    QPixmap pix = scHelmet;
    if (size != scHelmet.height())
        pix = QPixmap(":/ui_icons/sc_helmet.png").scaledToHeight(size, Qt::SmoothTransformation);

    int w = pix.width();

    return QRectF(-w/2 - 2, -pix.height()/2 - 2, w  + 4, pix.height() + 4);
}

void DriverTrackerSCItem::paint(QPainter *p, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if ((EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED) &&
        (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT))
    {
        p->setClipRect( option->exposedRect );

        updatePosition();

        int size = 40;

        if (viewScale >= 2.0)
        {
            size = 80;
        }
        else if (viewScale > 1.0)
        {
            size = 40 * viewScale;
        }

        if (size != scHelmet.height())
            scHelmet = QPixmap(":/ui_icons/sc_helmet.png").scaledToHeight(size, Qt::SmoothTransformation);

        QRectF bRect = boundingRect();
        p->drawPixmap(bRect.x(), bRect.y(), scHelmet);
    }
}

void DriverTrackerSCItem::getLeaderDriver()
{
    leaderDriver = dtTimer->getLeaderDriver();
}

void DriverTrackerSCItem::updatePosition()
{
    if (!leaderDriver || (leaderDriver->getDriverData()->getPosition() != 1))
        getLeaderDriver();

//    int idx = (int)floor(currentDeg);//(int)round(currentDeg);

    //amount of seconds that safety car runs ahead of the leader
    int secondsAhead = 3;

    double currentDeg = leaderDriver->getCurrentDeg();
    double avgTime = leaderDriver->getAvgTime();
    int coordinatesCount = leaderDriver->getCoordinatesCount();

    int mapX, mapY, mapW, mapH;
    leaderDriver->getMapCoordinates(&mapX, &mapY, &mapW, &mapH);

    currentDeg += ((double)secondsAhead * ((double)coordinatesCount / avgTime));

    if (currentDeg > (double)coordinatesCount)
        currentDeg -= (double)coordinatesCount;

    int idx = (int)floor(currentDeg);

    if ((!leaderDriver->getDriverData()->getLapData().isEmpty() &&
        !leaderDriver->getDriverData()->getLapData().last().getRaceLapExtraData().isSCLap()) ||
        leaderDriver->isInPits())
    {
        const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();

        if (trackCoordinates)
        {
            double resX = mapX -(mapX + mapW /2) + trackCoordinates->coordinates[0].x();
            double resY = -(mapY + mapH /2) + trackCoordinates->coordinates[0].y();

            if (resX != x() || resY != y())
            {
                setPos(resX, resY);
            }
        }
        return;
    }


    double factor = currentDeg - (double)idx;

    if (idx >= (coordinatesCount - 1))
        idx = coordinatesCount - 2;

    const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();

    if (trackCoordinates)
    {
        double newX = mapX -(mapX + mapW /2) + trackCoordinates->coordinates[idx].x();
        double newY = -(mapY + mapH /2) + trackCoordinates->coordinates[idx].y();

        double nextX = mapX -(mapX + mapW /2) + trackCoordinates->coordinates[idx+1].x();
        double nextY = -(mapY + mapH /2) + trackCoordinates->coordinates[idx+1].y();

        double resX = newX + factor * (nextX - newX);
        double resY = newY + factor * (nextY - newY);

        if (resX != x() || resY != y())
        {
            setPos(resX, resY);
        }
    }
}

}
