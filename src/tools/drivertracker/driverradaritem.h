/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERRADARITEM_H
#define DRIVERRADARITEM_H

#include <QGraphicsObject>

#include "driverradarpositioner.h"

namespace DriverTrackerTool
{

class DriverRadarItem : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit DriverRadarItem(DriverRadarPositioner *positioner, QGraphicsItem *parent = 0);
    virtual void setFlags();

    void setDriverRadarPositioner(DriverRadarPositioner *positioner);
    
    QRectF boundingRect () const;
    void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual bool isSelected()
    {
        return selected;
    }
    virtual void setSelected(bool sel)
    {
        selected = sel;

        if (selected)
            setZValue(2);
        else
            setZValue(1);

        update();
    }

    virtual void setViewScale(double)
    {

    }

    virtual void setViewRotation(int)
    {
    }

    void updateScaleAndRotation()
    {
    }

    bool isExcluded()
    {
        return excluded;
    }

    virtual void setExcluded(bool ex)
    {
        excluded = ex;
        setVisible(!excluded);
    }


    void setRadarCoords(int x, int y, double r, double rP, double rL)
    {
        if (drPositioner)
        {
            drPositioner->setRadarCoords(x, y, r, rP, rL);
            update();
        }
    }

    int getDriverId()
    {
        if (drPositioner)
            return drPositioner->getDriverId();

        return -1;
    }

    void setStartupPosition()
    {
        if (drPositioner)
            drPositioner->setStartupPosition();
    }

signals:
    void driverSelected(DriverRadarItem *driver);

public slots:
    void updatePosition()
    {
        if (drPositioner && isVisible())
        {
            setPos(drPositioner->getPos());
        }
    }

    void update()
    {
        QGraphicsItem::update();
    }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    DriverRadarPositioner *drPositioner;

    double viewScale;
    int viewRotation;

    bool selected;
    bool excluded;

    QPixmap label[4];

    enum labelType
    {
        NORMAL = 0,
        SELECTED,
        LEADER,
        RETIRED
    };
};

}
#endif // DRIVERRADARITEM_H
