/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "drivertrackertimer.h"
#include "../../core/eventdata.h"

namespace DriverTrackerTool
{

DriverTrackerTimer::DriverTrackerTimer(QObject *parent) :
    QObject(parent), fps(1), sessionInterval(1000)
{
    timer = new QTimer(parent);

    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));

    this->parent = parent;
    loadDriversList();

    intervalData.resetData();
}


void DriverTrackerTimer::startTimer(int s, bool reducedSpeed)
{
    if (timer->isActive())
    {
        setTimerInterval(s);
    }
    else
    {
        sessionInterval = s;
        int value = reducedSpeed ? sessionInterval : round(double(sessionInterval/fps));
        intervalData.resetData();

        timer->start(value);
    }
}

void DriverTrackerTimer::loadDriversList()
{
    for (int i = 0; i < dtp.size(); ++i)
    {
        delete drp[i];
        delete dtp[i];
    }

    drp.resize(EventData::getInstance().getDriversData().size());
    dtp.resize(EventData::getInstance().getDriversData().size());

    //drivers are added in reverse order, so that the leaders will be always on top
    for (int i = dtp.size()-1; i >= 0; --i)
    {
        drp[i] = new DriverRadarPositioner(parent, &EventData::getInstance().getDriversData()[i], fps);
        dtp[i] = new DriverTrackerPositioner(parent, &EventData::getInstance().getDriversData()[i], fps);
    }
}

/*!
 * On slower PC's currently set FPS may be too high. This function calculates the real FPS
 * (by measuring how many ms passed between two timer timeouts) and updates driver positioners
 * FPS data, so that the animation will be slower, but there won't be "high jumps" between
 * synchronization points
 */
void DriverTrackerTimer::calculateRealFPS()
{
    qint64 currentTick = QDateTime::currentMSecsSinceEpoch();

    if (intervalData.previousTick != 0)
    {
        qint64 timePassed = currentTick - intervalData.previousTick;

        ++intervalData.timeoutCounter;

        intervalData.avgInterval += timePassed;

        if (intervalData.timeoutCounter == 10)
        {
            intervalData.avgInterval /= intervalData.timeoutCounter;            

            double percent = intervalData.prevAvgInterval * 0.05;
            if (((double)qAbs(intervalData.avgInterval - intervalData.prevAvgInterval) > percent) &&
                ((intervalData.avgInterval > timer->interval()) || (intervalData.prevAvgInterval < timer->interval())) &&
                (intervalData.avgInterval <= 1000))
            {
                double fps = round((double)sessionInterval / (double)intervalData.avgInterval);
                qDebug() << "SETTING: " << sessionInterval << timer->interval() << intervalData.avgInterval << fps;
                setFPS((int)fps, true);
                intervalData.prevAvgInterval = intervalData.avgInterval;
            }
            intervalData.avgInterval = 0;
            intervalData.timeoutCounter = 0;
        }
    }
    intervalData.previousTick = currentTick;
}

DriverTrackerPositioner *DriverTrackerTimer::getLeaderDriver()
{
    for (int i = 0; i < dtp.size(); ++i)
    {
        if (dtp[i]->getDriverData()->getPosition() == 1)
            return dtp[i];
    }

    return NULL;
}


void DriverTrackerTimer::timeout()
{   
    for (int i = 0; i < dtp.size(); ++i)
    {
        drp[i]->updatePosition();
        dtp[i]->updatePosition();
    }   
    emit positionsUpdated();

    calculateRealFPS();
}
}
