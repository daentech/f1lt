/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverradaritem.h"

#include "../../core/eventdata.h"
#include "../../core/colorsmanager.h"

#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>


namespace DriverTrackerTool
{

DriverRadarItem::DriverRadarItem(DriverRadarPositioner *positioner, QGraphicsItem *parent) :
    QGraphicsObject(parent), viewScale(1.0), viewRotation(0), selected(false), excluded(false)
{
    label[NORMAL] = QPixmap(":/ui_icons/label-driver.png").scaledToHeight(12, Qt::SmoothTransformation);
    label[SELECTED] = QPixmap(":/ui_icons/label-driver-sel.png").scaledToHeight(12, Qt::SmoothTransformation);
    label[LEADER] = QPixmap(":/ui_icons/label-driver-leader.png").scaledToHeight(12, Qt::SmoothTransformation);
    label[RETIRED] = QPixmap(":/ui_icons/label-driver-retired.png").scaledToHeight(12, Qt::SmoothTransformation);

    setDriverRadarPositioner(positioner);
}

void DriverRadarItem::setFlags()
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
}


void DriverRadarItem::setDriverRadarPositioner(DriverRadarPositioner *positioner)
{
    if (positioner)
    {
        drPositioner = positioner;
        connect (drPositioner, SIGNAL(statusChanged()), this, SLOT(update()), Qt::UniqueConnection);
    }
}

void DriverRadarItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        if (!selected)
        {
            selected = true;
            emit driverSelected(this);
            setZValue(2);
        }
        else
        {
            selected = false;
            setZValue(1);
            emit driverSelected(NULL);
        }
        update();
    }
}

QRectF DriverRadarItem::boundingRect() const
{
    if (drPositioner && drPositioner->getDriverData() && drPositioner->getDriverData()->getCarID() > 0)
    {
        int size = 24;

        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), size);

        int w = helmet.width() > label[0].width() ? helmet.width() : label[0].width();

        return QRectF(-w/2 - 4, -helmet.height()/2, w + 8, helmet.height() + 4 + label[0].height());
    }
    return QRectF();
}

void DriverRadarItem::paint (QPainter *p, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if (!drPositioner || !drPositioner->getDriverData() || (drPositioner->getDriverData()->getCarID() < 1))
        return;

    LTPackets::EventType eType = EventData::getInstance().getEventType();
    if (!excluded && (eType != LTPackets::RACE_EVENT || !drPositioner->getDriverData()->isRetired()))
    {
        p->setClipRect( option->exposedRect );
        drPositioner->setTrackPosition();
        setPos(drPositioner->getPos());


        QColor drvColor = ColorsManager::getInstance().getCarColor(drPositioner->getDriverData()->getNumber());
        p->setBrush(QBrush(drvColor));

        QPen namePen(QPen(ColorsManager::getInstance().getColor(LTPackets::WHITE)));
        QPen rectPen(drvColor);

        QPixmap drvLabel = label[NORMAL];

        if (drPositioner->getDriverData()->getPosition() == 1)
        {
            drvLabel = label[LEADER];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        if (drPositioner->getDriverData()->isRetired() || drPositioner->isQualiOut())
        {
            drvLabel = label[RETIRED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        if (selected)
        {
            drvLabel = label[SELECTED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }

        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), 24);
        p->drawPixmap(-helmet.width()/2, boundingRect().y(), helmet);


        p->setPen(rectPen);
        int labelX = -drvLabel.width() / 2;
        int labelY = boundingRect().y() + helmet.height() + 4;

        static const QPointF points[4] = {
             QPointF(labelX + 8.0, labelY + 1.0),
             QPointF(labelX + 20.0, labelY + 1.0),
             QPointF(labelX + 20.0, labelY + drvLabel.height() - 1.0),
             QPointF(labelX + 1.0, labelY + drvLabel.height() - 1.0)
         };


        p->drawConvexPolygon(points, 4);

        p->setFont(QFont("Arial", 8, 75));

        QString name = SeasonData::getInstance().getDriverShortName(drPositioner->getDriverData()->getDriverName());//QString::number(driverData->getNumber());

        p->setPen(namePen);

        p->drawPixmap(-drvLabel.width()/2, labelY, drvLabel);


        int numX = /*point.x()*/ boundingRect().x() + 8 + (drvLabel.width() - p->fontMetrics().width(name))/2;
        int numY = /*point.y() +*/ labelY + (drvLabel.height() + p->fontMetrics().height())/2 - 1;
        p->drawText(numX, numY, name);

    }
}



}
