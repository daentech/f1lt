/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERTRACKER_H
#define DRIVERTRACKER_H

#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsSceneWheelEvent>
#include <QPixmap>
#include "track.h"
#include "pitlane.h"
#include "driverclassification.h"

namespace DriverTrackerTool
{

class DriverTracker : public QGraphicsScene
{
    Q_OBJECT

public:
    DriverTracker(QObject *parent = 0);

    virtual void loadDriversList();
    virtual void setupDrivers(int speed);

    virtual void checkSetupCorrect(int speed)
    {
        track->checkSetupCorrect(speed);
    }

    void setSpeed(int speed)
    {
        track->setSpeed(speed);
    }

    void setDrawDriverClassification(bool val)
    {
        drawClassification = val;
//        setup();
    }
    bool drawDriverClassification()
    {
        return drawClassification;
    }

    void rotate(int currentAngle, int totalAngle)
    {
        track->rotate(currentAngle);
        track->setViewRotation(totalAngle);

        correctTrackPosition();
    }

    void scale(double currentScale, double totalScale)
    {
        track->scale(currentScale, currentScale);
        track->setViewScale(totalScale);

        correctTrackPosition();
    }

    void correctTrackPosition()
    {
        track->setPos(driverClassificaction->x() + (driverClassificaction->boundingRect().width() + track->boundingRect().width())/2 + 50, 0);
        int x = track->x() - track->boundingRect().width()/2;

        if (x <= (driverClassificaction->boundingRect().x() + driverClassificaction->boundingRect().width()/2))
            track->setPos(driverClassificaction->x() + (driverClassificaction->boundingRect().width() + track->sceneBoundingRect().width())/2 + 50, 0);
    }

    void updatePosition()
    {
        track->updatePosition();
//        driverClassificaction->update();
    }

    DriverTrackerPositioner *getSelectedDriver()
    {
        return track->getSelectedDriver();
    }

public slots:
    void selectDriver(int id)
    {
        track->selectDriver(id);
        driverClassificaction->selectDriver(id);
    }

    void selectDriver(DriverTrackerPositioner *driver)
    {
        if (driver)
            driverClassificaction->selectDriver(driver->getDriverId());
        else
            driverClassificaction->selectDriver(-1);

        emit driverSelected(driver);
    }

    void sessionTimerUpdated()
    {
        driverClassificaction->update();
    }

signals:

    void driverSelected(int);
    void driverSelected(DriverTrackerPositioner*);
    void driverExcluded(int, bool);
    void viewScaled(int);

protected:

    void wheelEvent(QGraphicsSceneWheelEvent *event);

    bool isExcluded(int id);


    QList<int> excludedDrivers;

    bool drawClassification;

    Track *track;
    PitLane *pitLane;
    DriverClassification *driverClassificaction;
};

}
#endif // DRIVERTRACKER_H
