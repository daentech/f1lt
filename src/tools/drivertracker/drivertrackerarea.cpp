/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "drivertrackerarea.h"
#include <QPainter>
#include <QDebug>

#include "../../core/eventdata.h"

#include <QGraphicsPixmapItem>

namespace DriverTrackerTool
{

DriverTrackerArea::DriverTrackerArea(DriverTrackerTimer *timer, QGraphicsItem *parent) :
    DriverRadarArea(timer, parent)
{
    scItem = new DriverTrackerSCItem(timer, this);
    setCacheMode(QGraphicsItem::DeviceCoordinateCache) ;
}

QRectF DriverTrackerArea::boundingRect() const
{
    int w = trackMap.width();
    int h = trackMap.height();

    return QRectF(-w/2.0, -h/2.0, w, h);
}

void DriverTrackerArea::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!trackMap.isNull())
    {
        int w = trackMap.width();
        int h = trackMap.height();

        painter->setRenderHint(QPainter::SmoothPixmapTransform);
        painter->drawPixmap(- w/2, - h/2, trackMap);
    }
}

void DriverTrackerArea::setup()
{
    trackMap = EventData::getInstance().getEventInfo().trackImg;

    if (selectedDriver != -2)
        selectedDriver = -1;

    for (int i = 0; i < drp.size(); ++i)
    {
        static_cast<DriverTrackerItem*>(drp[i])->setMapCoords(scenePos().x(), scenePos().y(), trackMap.width(), trackMap.height());        
        drp[i]->setStartupPosition();        
        drp[i]->setExcluded(false);
        scItem->setVisible(false);
    }
}

void DriverTrackerArea::checkSetupCorrect()
{
    trackMap = EventData::getInstance().getEventInfo().trackImg;

    if (drp.isEmpty() || (selectedDriver == -2))
    {
        setup();
    }
    else
    {
        for (int i = 0; i < drp.size(); ++i)
        {
            drp[i]->updateScaleAndRotation();
        }

        if (scItem->isVisible())
            scItem->updateScaleAndRotation();
    }
}

void DriverTrackerArea::loadDriversList()
{
    selectedDriver = -1;
    for (int i = 0; i < drp.size(); ++i)
        delete drp[i];

    drp.resize(EventData::getInstance().getDriversData().size());

    //drivers are added in reverse order, so that the leaders will be always on top
    for (int i = drp.size()-1; i >= 0; --i)
    {
        drp[i] = new DriverTrackerItem(dtTimer->getDriverTrackerPositioner(i), this);//, &EventData::getInstance().getDriversData()[i]);
        drp[i]->setFlags();
        connect (drp[i], SIGNAL(driverSelected(DriverTrackerItem*)), this, SLOT(onDTPDriverSelected(DriverTrackerItem*)), Qt::UniqueConnection);
    }
}

void DriverTrackerArea::updatePosition()
{
    DriverRadarArea::updatePosition();

    if (scItem->isVisible())
        scItem->updatePosition();

    if (!scItem->isVisible() && (EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED))
    {
        scItem->setVisible(true);
        scItem->update();
    }
    else if (scItem->isVisible() && (EventData::getInstance().getFlagStatus() != LTPackets::SAFETY_CAR_DEPLOYED))
    {
        scItem->setVisible(false);
        scItem->update();
    }
}

DriverTrackerItem *DriverTrackerArea::getSelectedDriver()
{
    if (selectedDriver == -1)
        return NULL;

    for (int i = 0; i < drp.size(); ++i)
    {
        if (drp[i]->getDriverId() == selectedDriver)
            return static_cast<DriverTrackerItem*>(drp[i]);
    }

    return NULL;
}

void DriverTrackerArea::onDTPDriverSelected(DriverTrackerItem *driver)
{
    if (driver)
    {
        /* A single press can be detected by several drivers (if they are close to each other), but only one can be selected
         * at a time. So all others have to be deselected then
         */
        for (int i = 0; i < drp.size(); ++i)
        {
            if (drp[i] != driver)
            {
                drp[i]->setSelected(false);
            }
        }


        selectedDriver = driver->getDriverId();
    }
    else
    {
        selectedDriver = -1;
    }
    emit driverSelected(selectedDriver);
}

void DriverTrackerArea::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    /* This function checks whether a mouse button has been pressed somewhere on the track, but not on any driver.
     * If yes - all drivers have to be deselected and appropriate signal has to be emitted to inform driverTrackerWidget that
     * there is no driver to follow
     */
    if (event->button() == Qt::LeftButton)
    {
        bool driverFound = false;
        for (int i = 0; i < drp.size(); ++i)
        {
            if (!drp[i]->contains(event->pos()))
                drp[i]->setSelected(false);
            else
                driverFound = true;
        }

        if (!driverFound)
            emit driverSelected(-1);
    }
}




}
