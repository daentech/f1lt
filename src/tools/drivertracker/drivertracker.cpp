/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "drivertracker.h"
#include "drivertrackerpositioner.h"

#include <QMouseEvent>

#include "../../core/colorsmanager.h"
#include "../../main_gui/models/qualimodel.h"

namespace DriverTrackerTool
{

DriverTracker::DriverTracker(QObject *parent) : QGraphicsScene(parent), drawClassification(true)
{
    track = new Track(0, this);
    pitLane = new PitLane();
    driverClassificaction = new DriverClassification(0, this);

    setItemIndexMethod(QGraphicsScene::NoIndex);

    loadDriversList();
    connect (track, SIGNAL(driverSelected(DriverTrackerPositioner*)), this, SLOT(selectDriver(DriverTrackerPositioner*)), Qt::UniqueConnection);
    connect (driverClassificaction, SIGNAL(driverSelected(int)), this, SIGNAL(driverSelected(int)));
    connect (driverClassificaction, SIGNAL(driverExcluded(int,bool)), this, SIGNAL(driverExcluded(int,bool)));
    connect (driverClassificaction, SIGNAL(driverExcluded(int,bool)), track, SLOT(excludeDriver(int,bool)));
}


void DriverTracker::setupDrivers(int speed)
{
    track->setup(speed);

//    addItem(pitLane);
    addItem(track);
    addItem(driverClassificaction);

    excludedDrivers.clear();

    driverClassificaction->setPos(sceneRect().x()+20, 0);
//    pitLane->setPos(sceneRect().x()+25, driverClassificaction->y() + (driverClassificaction->boundingRect().height() + pitLane->boundingRect().height())/2 + 20);

    track->setPos(driverClassificaction->x() + (driverClassificaction->boundingRect().width() + track->boundingRect().width())/2 + 50, 0);
}



void DriverTracker::loadDriversList()
{
    track->loadDriversList();
}



bool DriverTracker::isExcluded(int id)
{
    for (int i = 0; i < excludedDrivers.size(); ++i)
    {
        if (excludedDrivers[i] == id)
            return true;
    }

    return false;
}

void DriverTracker::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    if (event->modifiers() == Qt::ControlModifier)
    {
        emit viewScaled(event->delta()/60);
    }
}

}
