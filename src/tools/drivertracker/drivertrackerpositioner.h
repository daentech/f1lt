/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERTRACKERPOSITIONER_H
#define DRIVERTRACKERPOSITIONER_H

#include "driverradarpositioner.h"
//#include <QGraphicsItem>
//#include <QGraphicsSceneMouseEvent>

namespace DriverTrackerTool
{

class DriverTrackerPositioner : public DriverRadarPositioner
{
    Q_OBJECT
public:
    DriverTrackerPositioner(QObject *parent = 0, DriverData *dd = 0, int speed = 40);
    virtual ~DriverTrackerPositioner() { }    


    void setTrackPosition();

    virtual void setStartupPosition();
    virtual void calculatePosition();

//    virtual void setSynchronizationPointOnTrackReached();
//    virtual void detectSynchronizationPointOnMapReached();

    void setMapCoords(int x, int y, int w, int h)
    {
        mapX = x;
        mapY = y;
        mapW = w;
        mapH = h;
    }

    virtual int sectorIndex(int sector)
    {
        if (trackCoordinates)
            return trackCoordinates->indexes[sector];

        return 0;
    }

    virtual int numberOfIndexes() const
    {
        return coordinatesCount;
    }

    virtual void calculatePitOutPosition();    

    int getCoordinatesCount() const
    {
        return coordinatesCount;
    }

    void getMapCoordinates(int *x, int *y, int *width, int *height) const
    {
        if (x && y && width && height)
        {
            *x = mapX;
            *y = mapY;
            *width = mapW;
            *height = mapH;
        }
    }


protected:

signals:

private:
    int coordinatesCount;
    int mapX, mapY, mapW, mapH;
    const TrackCoordinates *trackCoordinates;

};

}
#endif // DRIVERTRACKERPOSITIONER_H
