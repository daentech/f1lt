/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "drivertrackeritem.h"

#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>

#include "../../core/colorsmanager.h"

namespace DriverTrackerTool
{

DriverTrackerItem::DriverTrackerItem(DriverRadarPositioner *positioner, QGraphicsItem *parent) :
    DriverRadarItem(positioner, parent)
{
}

void DriverTrackerItem::setFlags()
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}


QRectF DriverTrackerItem::boundingRect() const
{
    if (drPositioner && drPositioner->getDriverData() && drPositioner->getDriverData()->getCarID() > 0)
    {
        int size = 24;

        if (viewScale >= 2.0)
        {
            size = 48;
        }
        else if (viewScale > 1.0)
        {
            size = 24 * viewScale;
        }
        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), size);

        int w = helmet.width() > label[0].width() ? helmet.width() : label[0].width();

        return QRectF(-w/2 - 9, -helmet.height()/2, w  + 18, helmet.height() + 8 + label[0].height());
    }
    return QRectF();
}

void DriverTrackerItem::rescaleLabels()
{
    int size = 12;

    if (viewScale >= 2.0)
    {
        size = 24;
    }
    else if (viewScale > 1.0)
    {
        size = 12 * viewScale;
    }

    label[NORMAL] = QPixmap(":/ui_icons/label-driver.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[SELECTED] = QPixmap(":/ui_icons/label-driver-sel.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[LEADER] = QPixmap(":/ui_icons/label-driver-leader.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[RETIRED] = QPixmap(":/ui_icons/label-driver-retired.png").scaledToHeight(size, Qt::SmoothTransformation);
}

void DriverTrackerItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
//    sprawdzic to!!
    if ((event->button() == Qt::LeftButton) && drPositioner && !drPositioner->isInPits())
    {
        if (!selected)
        {
            selected = true;
            emit driverSelected(this);
            setZValue(2);
            update();
        }
        else
        {
            selected = false;
            setZValue(1);
            emit driverSelected(NULL);
            update();
        }
    }
}

void DriverTrackerItem::paint(QPainter *p, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if (drPositioner && drPositioner->getDriverData() && (drPositioner->getDriverData()->getCarID() > 0) &&
         !drPositioner->isInPits() && !drPositioner->getDriverData()->isRetired() && !excluded && !drPositioner->isFinished())
    {
        p->setClipRect( option->exposedRect );
        drPositioner->setTrackPosition();
        setPos(drPositioner->getPos());

        QColor drvColor = ColorsManager::getInstance().getCarColor(drPositioner->getDriverData()->getNumber());
        p->setBrush(QBrush(drvColor));

        QPen namePen(QPen(ColorsManager::getInstance().getColor(LTPackets::WHITE)));
        QPen rectPen(drvColor);

        QPixmap drvLabel = label[NORMAL];

        if (drPositioner->getDriverData()->getPosition() == 1)
        {
            drvLabel = label[LEADER];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        if (drPositioner->getDriverData()->isRetired() || drPositioner->isQualiOut())
        {
            drvLabel = label[RETIRED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        if (selected)
        {
            drvLabel = label[SELECTED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }

        int size = 24;

        if (viewScale >= 2.0)
        {
            size = 48;
        }
        else if (viewScale > 1.0)
        {
            size = 24 * viewScale;
        }

        p->setFont(QFont("Arial", size / 3, 75));

        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), size);
        p->drawPixmap(-size/2, boundingRect().y()+1, helmet);

        p->setPen(rectPen);
        int labelX = -drvLabel.width() / 2;
        int labelY = boundingRect().y() + helmet.height() + 5;

        int labelSize = size / 2;
        const QPointF points[4] = {
             QPointF(labelX + 2.0 + labelSize/2, labelY + 1.0),
             QPointF(labelX + labelSize + 8.0, labelY + 1.0),
             QPointF(labelX + labelSize + 8.0, labelY + drvLabel.height() - 2.0),
             QPointF(labelX + 1.0, labelY + drvLabel.height() - 2.0)
         };


        p->drawConvexPolygon(points, 4);
        p->drawPixmap(-drvLabel.width()/2, labelY, drvLabel);

        QString name = SeasonData::getInstance().getDriverShortName(drPositioner->getDriverData()->getDriverName());//QString::number(driverData->getNumber());

        p->setPen(namePen);

        int numX = labelX + labelSize/3 + (drvLabel.width() - p->fontMetrics().width(name))/2;
        int numY = labelY + (drvLabel.height() + p->fontMetrics().height())/2 - 2;
        p->drawText(numX, numY, name);
    }
}

}
