/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "drivertrackerwidget.h"
#include "ui_drivertrackerwidget.h"

#include <QDebug>
#include <QKeyEvent>
//#include <QGLWidget>

namespace DriverTrackerTool
{

DriverTrackerWidget::DriverTrackerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DriverTrackerWidget)
{
    ui->setupUi(this);

//    QGLWidget *glWidget = new QGLWidget(this);

    selectedDriver = 0;

    driverTrackerTimer = new DriverTrackerTimer(this);
    driverTrackerScene = new DriverTrackerScene(driverTrackerTimer, this);
    driverRadarScene = new DriverRadarScene(driverTrackerTimer, this);

    ui->driverRadarView->setScene(driverRadarScene);
    ui->driverTrackerView->setScene(driverTrackerScene);

//    ui->driverTrackerView->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));
//    ui->driverRadarView->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));
    ui->driverTrackerView->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
    ui->driverRadarView->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
//    ui->driverTrackerView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
//    ui->driverRadarView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    ui->driverTrackerView->setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing | QGraphicsView::DontSavePainterState);
    ui->driverRadarView->setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing | QGraphicsView::DontSavePainterState);

    ui->driverTrackerView->setCacheMode(QGraphicsView::CacheBackground);
    ui->driverRadarView->setCacheMode(QGraphicsView::CacheBackground);

    connect (driverTrackerTimer, SIGNAL(positionsUpdated()), this, SLOT(update()));
    connect (this, SIGNAL(timerStarted(int,bool)), driverTrackerTimer, SLOT(startTimer(int,bool)));
    connect (driverRadarScene, SIGNAL(driverSelected(int)), this, SLOT(onRadarDriverSelected(int)));
    connect (driverTrackerScene, SIGNAL(driverSelected(int)), this, SLOT(onTrackerDriverSelected(int)));

    connect (driverTrackerScene, SIGNAL(driverExcluded(int,bool)), driverRadarScene, SLOT(excludeDriver(int,bool)));
    connect (ui->driverTrackerView, SIGNAL(viewScaled(int)), this, SLOT(viewScaled(int)));
}

DriverTrackerWidget::~DriverTrackerWidget()
{
    delete ui;
}

void DriverTrackerWidget::update()
{
    driverRadarScene->updatePosition();
    driverTrackerScene->updatePosition();

    if (selectedDriver)
        ui->driverTrackerView->centerOn(selectedDriver);
}

void DriverTrackerWidget::viewScaled(int delta)
{
    int max = ui->zoomSlider->maximum();
    int min = ui->zoomSlider->minimum();

    double zoom = ui->zoomValue->text().toDouble() * 10.0 + delta;
    if (zoom > max)
        zoom = max;

    if (zoom < min)
        zoom = min;

    ui->zoomSlider->setValue(zoom);
}

void DriverTrackerWidget::setup()
{    
    selectedDriver = 0;
    driverTrackerTimer->loadDriversList();
    driverRadarScene->loadDriversList();
    driverTrackerScene->loadDriversList();

    driverRadarScene->setupDrivers();
    driverTrackerScene->setupDrivers();

    resetView();
    setWindowTitle("Driver tracker: " + EventData::getInstance().getEventInfo().eventName);    
}

void DriverTrackerWidget::loadSettings(QSettings *settings)
{
    ui->splitter->restoreState(settings->value("ui/tracker_splitter_pos").toByteArray());
    restoreGeometry(settings->value("ui/driver_tracker_geometry").toByteArray());

    ui->fpsSlider->setValue(settings->value("ui/tracker_fps", 25).toInt());
}

void DriverTrackerWidget::saveSettings(QSettings *settings)
{
    settings->setValue("ui/tracker_splitter_pos", ui->splitter->saveState());
    settings->setValue("ui/driver_tracker_geometry", saveGeometry());
    settings->setValue("ui/tracker_fps", ui->fpsSlider->value());
}



void DriverTrackerWidget::on_pushButton_clicked()
{   
    close();
}

void DriverTrackerWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        close();
}

void DriverTrackerWidget::closeEvent(QCloseEvent *event)
{
    //set FPS to 1 to decrease CPU load
    //reset timer speed
    driverTrackerTimer->setFPS(1);

    QWidget::closeEvent(event);
}

void DriverTrackerWidget::resizeEvent(QResizeEvent *event)
{
    driverRadarScene->updateSize();
    QWidget::resizeEvent(event);
}

void DriverTrackerWidget::exec()
{
    setWindowTitle("Driver tracker: " + EventData::getInstance().getEventInfo().eventName);

    //set speed back to default
    int fps = ui->fpsValue->text().toInt();

    driverTrackerScene->setTrackPosition();

    show();    

    //reset timer speed
    driverTrackerTimer->setFPS(fps);

    driverRadarScene->checkSetupCorrect();
    driverTrackerScene->checkSetupCorrect();
}

void DriverTrackerWidget::startTimer(int s)
{
    if (driverTrackerTimer->isTimerActive())
    {
        driverTrackerTimer->setTimerInterval(s, !isVisible());
    }

    else
    {
        driverTrackerScene->checkSetupCorrect();
        driverRadarScene->checkSetupCorrect();

        driverTrackerTimer->startTimer(s, !isVisible());
    }
}

void DriverTrackerWidget::sessionTimerUpdated()
{
    //this slot will be called by session timer to update for example classification
    driverTrackerScene->sessionTimerUpdated();
    driverRadarScene->sessionTimerUpdated();
}

void DriverTrackerWidget::onRadarDriverSelected(int id)
{
    driverTrackerScene->selectDriver(id);

    if (id != -1)
    {
        selectedDriver = driverTrackerScene->getSelectedDriver();

        if (selectedDriver && selectedDriver->isVisible())
            ui->driverTrackerView->centerOn(selectedDriver);
    }
    else
        selectedDriver = 0;
}

void DriverTrackerWidget::onTrackerDriverSelected(int id)
{    
    driverRadarScene->selectDriver(id);

    if (id != -1)
    {
        selectedDriver = driverTrackerScene->getSelectedDriver();

        if (selectedDriver)
            ui->driverTrackerView->centerOn(selectedDriver);
    }
    else
        selectedDriver = 0;
}

void DriverTrackerWidget::resetView()
{
    driverTrackerScene->resetView();
    ui->zoomValue->setText("1.0");
    ui->zoomSlider->setValue(10);

    ui->rotationValue->setText("0");
    ui->rotationSlider->setValue(0);
}

}

void DriverTrackerTool::DriverTrackerWidget::on_zoomSlider_valueChanged(int value)
{
    double currentScale = ui->zoomValue->text().toDouble();
    double newScale = double(value / 10.0);
//    ui->driverTrackerView->resetMatrix();

//    ui->driverTrackerView->scale(newScale/currentScale, newScale/currentScale);

    driverTrackerScene->scale(newScale/currentScale, newScale);

    ui->zoomValue->setText(QString::number(newScale, 'f', 1));

    //reset timer speed
    driverTrackerTimer->setFPS(ui->fpsSlider->value());
}

void DriverTrackerTool::DriverTrackerWidget::on_rotationSlider_valueChanged(int value)
{
    int currentRotation = ui->rotationValue->text().toInt();
    int newRotation = value;

    qreal angle = newRotation - currentRotation;

    driverTrackerScene->rotate(angle, value);

    ui->rotationValue->setText(QString::number(newRotation));

    //reset timer speed
    driverTrackerTimer->setFPS(ui->fpsSlider->value());
}

void DriverTrackerTool::DriverTrackerWidget::on_fpsSlider_valueChanged(int value)
{
    ui->fpsValue->setText(QString::number(value));
    int fps = value;

    //reset timer speed
    driverTrackerTimer->setFPS(fps);
}

void DriverTrackerTool::DriverTrackerWidget::on_splitter_splitterMoved(int, int )
{
    driverRadarScene->updateSize();
}

void DriverTrackerTool::DriverTrackerWidget::on_resetButton_clicked()
{
    resetView();
}
